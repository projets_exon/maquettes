<!doctype html>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Webcam </title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/Logo.css">
<link rel="stylesheet" href="assets/css/Navigation-Clean.css">
<link rel="stylesheet" href="assets/css/Social-Navi.css">
<link rel="stylesheet" href="assets/css/styles.css">
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'
href='https://fonts.googleapis.com/css?family=Permanent+Marker%3A400%7CPlayfair+Display%3A400%2C700italic%2C900italic%2C400italic&#038;subset=latin&#038;ver=1611958473'
type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="assets/css/tilteffect.css" />
<link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
<link rel="stylesheet" type="text/css" href="assets/css/zoomslider.css" />
	<script type="text/javascript" src="assets/js/modernizr-2.6.2.min.js"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Permanent Marker" !important;}
body, html {
  height: 100%;
  color: #777;
  font-family: "Permanent Marker" !important;
}

/* Create a Parallax Effect */
.bgimg-1, .bgimg-2, .bgimg-3 {
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* First image (Logo. Full height) */
.bgimg-1 {
  background-image: url('assets/img/dameblanche-damebg-1400x700v1-1024x512.jpg');
  min-height: 100%;
  max-height:1000px;
}

.w3-wide {letter-spacing: 10px;}
.w3-hover-opacity {cursor: pointer;}

/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1600px) {
  .bgimg-1, .bgimg-2, .bgimg-3 {
    background-attachment: scroll;
    min-height: 120%;
  }
  .padding-m-model{padding:20px;}
}
.space{width:100%;height:20px;}
.bg-map{
background-color:#ffffff;
background-size: 150% 100%;
min-height: auto;
max-height:auto;
height:auto;
padding-bottom:50px;
}
.map{
font-size:35px;
color:#000000;
}
.quiz-f-c{border:1px solid #0ccf05;width:100%;height:auto;padding:20px;}
.quiz-f-c h1{font-size:20px;color:#0ccf05;}
.bg-red{background-color:#7c0404;}
.quiz-f{padding:20px;text-align:center;}
.quiz-f label{color:#ffffff;}
.partage-img-d{text-align:center;}
.partage-img{width:40px;border-radius:10px;}
</style>
<style type="text/css">
form { margin-top: 15px; }
form > input { margin-right: 15px; }
#my_camera {padding:0px;min-width:100% !important;}
#results {padding:20px;min-width:100% !important;}
</style>
</head>
<body>




<div class="bgimg-1 w3-display-container" id="home">
<?php 
include "nav.php";
?>
<div class="row cont-page padding-m" style="margin-top:100px;">

<div class="bg-map">
<div>
<form style="margin-top:10px;text-align:center;">
<input type="button" value="Access Camera" onClick="setup(); $(this).hide().next().show();">
<input type="button" class="btn btn-primary" value="Prendre votre photo" onClick="take_snapshot()" style="display:none">
</form>

<div class="row">
<div class="col-md-6"><div id="my_camera"></div></div>
<div class="col-md-6"><div id="results"></div></div>
</div>
</div>
</div>
<div class="space"></div>
</div>
</div>



	
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="assets/cam/webcam.min.js"></script>
<script language="JavaScript">
		Webcam.set({
			width: 320,
			height: 240,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
</script>
<script language="JavaScript">
		function setup() {
			Webcam.reset();
			Webcam.attach( '#my_camera' );
		}
		
		function take_snapshot() {
			// take snapshot and get image data
			Webcam.snap( function(data_uri) {
				// display results in page
				document.getElementById('results').innerHTML = 
				'<img style="border:2px solid #000000;width:100%;" src="'+data_uri+'"/>' +
				'<div class="padding-m-model" style="margin-top:10px;padding:0px;"><div class="quiz-f-c"><h1>Vous pouvez partagez votre photo :</h1><div class="row"><div class="col-md-12 partage-img-d"><img class="partage-img" src="assets/img/whatsapp-square.png"/> <img class="partage-img" src="assets/img/twit.png"/> <img class="partage-img" src="assets/img/twit.png"/> <img class="partage-img" src="assets/img/whatsapp-square.png"/></div></div></div></br></br></div>';
			} );
		}
		

</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
