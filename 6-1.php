<!DOCTYPE html>
<html>
<title>Chasse au fantôme</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/Logo.css">
<link rel="stylesheet" href="assets/css/Navigation-Clean.css">
<link rel="stylesheet" href="assets/css/Social-Navi.css">
<link rel="stylesheet" href="assets/css/styles.css">
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'
href='https://fonts.googleapis.com/css?family=Permanent+Marker%3A400%7CPlayfair+Display%3A400%2C700italic%2C900italic%2C400italic&#038;subset=latin&#038;ver=1611958473'
type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="assets/css/tilteffect.css" />
<link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Permanent Marker" !important;}
body, html {
  height: 100%;
  color: #777;
  font-family: "Permanent Marker" !important;
}

/* Create a Parallax Effect */
.bgimg-1, .bgimg-2, .bgimg-3 {
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* First image (Logo. Full height) */
.bgimg-1 {
  background-image: url('assets/img/dameblanche-damebg-1400x700v1-1024x512.jpg');
  min-height: 100%;
  max-height:1000px;
}

.w3-wide {letter-spacing: 10px;}
.w3-hover-opacity {cursor: pointer;}

/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1600px) {
  .bgimg-1, .bgimg-2, .bgimg-3 {
    background-attachment: scroll;
    min-height: 120%;
  }
  .padding-m{padding:0px;}
  .padding-m-model{padding:10px;}
}
.space{width:100%;height:20px;}
.bg-map{
background-size: 150% 100%;
min-height: 100%;
max-height:350px;
height:350px;
}
.map{
font-size:35px;
border: 1px solid #212529;
}
.video{
width:100%;
height:100%;
}
</style>
<body>

<div class="bgimg-1 w3-display-container" id="home">
<?php 
include "nav.php";
?>
<div class="row cont-page w3-opacity-min padding-m" style="margin-top:100px;">
<div class="bg-map padding-m-model">
<iframe class="video" src="https://www.youtube.com/embed/ri8x8jqnLUs">
</iframe>
</div>
<div class="space"></div>
<div class="" style="text-align:center;">
<a class="btn btn-primary btn-danger btn-play" href="p7.php"> Suivant </a>
</div>
<div class="space"></div>
</div>
</div>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
