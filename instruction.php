<!DOCTYPE html>
<html>
<title>Chasse au fantôme Instruction</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/Logo.css">
<link rel="stylesheet" href="assets/css/Navigation-Clean.css">
<link rel="stylesheet" href="assets/css/Social-Navi.css">
<link rel="stylesheet" href="assets/css/styles.css">
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css' href='https://fonts.googleapis.com/css?family=Permanent+Marker%3A400%7CPlayfair+Display%3A400%2C700italic%2C900italic%2C400italic&#038;subset=latin&#038;ver=1611958473' type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="assets/css/tilteffect.css" />
<link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
<link rel="stylesheet" type="text/css" href="assets/css/zoomslider.css" />
	<script type="text/javascript" src="assets/js/modernizr-2.6.2.min.js"></script>
<style>
  body,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: "Permanent Marker" !important;
  }

  p {
    font-family: "Playfair Display" !important;
  }

  body,
  html {
    height: 100%;
    color: #777;
    font-family: "Permanent Marker" !important;
  }

  /* Create a Parallax Effect */
  .bgimg-1,
  .bgimg-2,
  .bgimg-3 {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  /* First image (Logo. Full height) */
  .bgimg-1 {
    background-image: url('assets/img/indienne-hammer-1024x512.jpg');
    min-height: 100%;
   
  }

  .w3-wide {
    letter-spacing: 10px;
  }

  .w3-hover-opacity {
    cursor: pointer;
  }

  .styleP {
    border: thick double rgb(176 51 63);
    padding: 10px;
  }


  /* Turn off parallax scrolling for tablets and phones */
  @media only screen and (max-device-width: 1600px) {

    .bgimg-1,
    .bgimg-2,
    .bgimg-3 {
      background-attachment: scroll;
      min-height: 100%;
    }
  }

  .space {
    width: 100%;
    height: 20px;
  }
  #bgimg-cont {
position: relative; /* can either be relative, absolute or fixed. If position is not set (i.e. static), it would be set to "relative" by script */
width: 100%;
min-height: 110%;
max-height:1500px;
background-color: #999;
}
</style>

<body>


<div id="bgimg-cont" data-zs-src='["assets/img/indienne-hammer-1024x512.jpg"]' data-zs-overlay="dots">    <?php
    include "nav.php";
    ?>
    <h1 class="col-t0-t">Instruction </h1>
    <div class="row cont-page w3-opacity-min" style="margin-top: 100px;">

      <p class="text-justify text-white styleP">
        A moins d’avoir deux cellulaires, il est préférable
        d’imprimer la carte pour suivre le parcours.</br>
        • Assurez vous d’avoir un lecteur de code Qr téléchargé
        dans votre téléphone ainsi que des données internet.</br>
        • Pour vivre pleinement l’expérience, rendez-vous dans le
        Vieux-Québec et faites le parcours à pied.
        Le départ se fait à la Batterie Royale.</br>
        • Il est fortement recommandé de faire la promenade en
        suivant les numéros indiqués sur la carte.</br>
        • Lorsque vous arrivez sur le point d’intérêt indiqué sur
        la carte, faites la lecture du code Qr associé au numéro.
        Vous aurez besoin du mot de passe qui vous a été fourni
        pour les ouvrir.</br>
        • Quatre vidéos vous donneront de petites missions pour
        vous aider à résoudre la formule mathématique sur la
        carte. La réponse est nécessaire pour ouvrir la dernière
        vidéo. Soyez attentifs!</br>
        • Visionner le vidéo à la verticale afin de l’avoir
        en plein écran.</br>
        • Les propos contenus dans les vidéos s’adressent aux
        10 ans et plus, le visionnement pour les plus jeunes est
        à votre discrétion.</br>
        Le mot de passe que vous avez reçu sera valide
        sur une durée de 48h.</br>
        Bonne promenade!
      </p>
      <div class="" style="text-align:center;">
        <a class="btn  btn-danger btn-play" href="p1.php"> J’accepte </a>
      </div>
      <div class="space"></div>
    </div>
  </div>


  <script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.zoomslider.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr-2.6.2.min.js"></script></body>
</body>

</html>