<!DOCTYPE html>
<html>
<title>Chasse au fantôme</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/Logo.css">
<link rel="stylesheet" href="assets/css/Navigation-Clean.css">
<link rel="stylesheet" href="assets/css/Social-Navi.css">
<link rel="stylesheet" href="assets/css/styles.css">
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'
href='https://fonts.googleapis.com/css?family=Permanent+Marker%3A400%7CPlayfair+Display%3A400%2C700italic%2C900italic%2C400italic&#038;subset=latin&#038;ver=1611958473'
type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="assets/css/tilteffect.css" />
<link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
<link rel="stylesheet" type="text/css" href="assets/css/zoomslider.css" />
	<script type="text/javascript" src="assets/js/modernizr-2.6.2.min.js"></script>

<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Permanent Marker" !important;}
body, html {
  height: 100%;
  color: #777;
  font-family: "Permanent Marker" !important;
}


/* Create a Parallax Effect */
.bgimg-1, .bgimg-2, .bgimg-3 {
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* First image (Logo. Full height) */
.bgimg-1 {
  background-image: url('assets/img/dameblanche-damebg-1400x700v1-1024x512.png');
  min-height: 100%;
  max-height:1500px;
}
.tailleSelect {
    width: 248px;
    height: 50px;
    text-align: center;
}
.w3-wide {letter-spacing: 10px;}
.w3-hover-opacity {cursor: pointer;}

/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1600px) {
  .bgimg-1, .bgimg-2, .bgimg-3 {
    background-attachment: scroll;
    min-height: 130%;
  }
}

#bgimg-cont {
position: relative; /* can either be relative, absolute or fixed. If position is not set (i.e. static), it would be set to "relative" by script */
width: 100%;
min-height: 110%;
max-height:1500px;
background-color: #999;
}
</style>
<body>


<div id="bgimg-cont" data-zs-src='["assets/img/dameblanche-damebg-1400x700v1-1024x512.png"]' data-zs-overlay="dots">
<?php 
include "nav.php";
?>
<div class="row cont-page w3-opacity-min" style="margin-top:100px;">
<h1 class="col-t0-t">Chasse aux Fantôme crime et chatiments</h1>
<div class="col col-12 col-t1">
<h1 class="col-t1-t">Bienvenue / Welcome </h1>
</div>
<div class="col col-12 col-t2">
<h3 class="col-t2-t">choisissiez votre Langue/ choose your Language :
</h3>
<div class="select-form">
<select class="tailleSelect">
<optgroup>
<option value="12" selected="" >Français</option>
<option value="13">Anglais</option>
</optgroup>
</select>
</div>

<div class="" style="padding:0px;margin:9px;text-align:center;margin-top:100px;height:0px;">
<a class="btn btn-primary btn-danger btn-play" href="./instruction.php" >Jouer / Play</a> 
</div>
</div>
</div>
</div>



<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.zoomslider.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr-2.6.2.min.js"></script></body>
</html>
